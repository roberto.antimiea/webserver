const express = require('express');
const socketio = require('socket.io');
const config = require('./config.js');

const app = express();


app.use(express.static('public'));

const server = app.listen(config.localport, () => {
    console.log('Server in ascolto sulla porta ' + config.localport);
});

const io = socketio(server);

io.on('connection', (socket) => {
    socket.join('room');
    console.log('client connesso');
    socket.emit('benvenuto');
    socket.on('send', ([messaggio, utente]) => {
        console.log(messaggio);
        io.emit('receive', [messaggio, utente]);
})
socket.on('registra', (messaggio) => {
    console.log(messaggio);
    socket.username = messaggio;

})
});

  




