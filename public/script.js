var socket = io();
var nome = '';

socket.on('welcome', (messaggio) => {
    document.getElementById('messages').innerText = messaggio;
});

function invia() {
    let messaggio = document.getElementById('username').value;
    socket.emit('send', [messaggio, nome]);
}

socket.on('receive', ([messaggio, utente]) => {
    var mex = document.createElement('span');
    mex.style.backgroundColor = '#666';
    var chat = document.getElementById('chat');
    mex.innerHTML = utente + ' ' + messaggio;
    chat.appendChild(mex);
    var br = document.createElement('br');
    chat.appendChild(br);
});


function registra(){
    let utente = document.getElementById('utente').value;
    socket.emit('registra', utente);
    nome = utente
}